/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package springlab1;

import java.util.Random;

/**
 *
 * @author basilides
 */
public class RoomPhysicks {
    
    public static int randInt(int min, int max)
    {
        Random rand = new Random();
        int randNum = rand.nextInt((max - min) + 1) + min;
        return randNum;
    }
    
       
    Integer temp;
    Integer light;
    
    Integer door;
    Integer window;
    
    Boolean lamp;
    Boolean cooler;
    
    Integer magic; // determines room i think?
    private final Integer amega;
    
    
    void setLamp(Boolean l)
    {
        lamp = l;      
    }
    
    void setCooler(Boolean c)
    {
        cooler = c;
    }
    
    public RoomPhysicks(Integer magic) {
        amega = 5;
        // отклонение 111!!!111
        this.magic = magic;
        temp = 20;
        light = 10;
        door = 0;
        window = 0;
        
        cooler = false;
        lamp = false;
        
    }
    
    Integer measure(String name)
    {
        switch(name)
        {
            case "temp" :
                return randInt(temp - amega + 10*(cooler ? 1 : 0), temp + amega + 10*(cooler ? 1 : 0));
            case "light":
                return randInt(light - amega + 10*(lamp ? 1 : 0), temp + amega + 10*(lamp ? 1 : 0));
            case "door":
                return door;
            case "window":
                return window;
        }
        return null;
    }
}
