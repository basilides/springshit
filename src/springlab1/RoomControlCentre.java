/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package springlab1;

import java.util.HashMap;
import java.util.Map;

interface Function<F, T> {

    public T apply(F from);
}

/**
 *
 * @author basilides он не знает ни о чем ему присылают сигналы он не знает, КТО
 * это можно обернуть в класс "сигнала" (например, он принимает их из вайфай или
 * еще как-то), но это оверинжиниринг
 */
public class RoomControlCentre {

    Map<String, Function<Integer, Void>> processor;
    RoomPhysicks rph;

    // температуры, с которых должны включаться / выключаться приборы
    Integer temp_on;
    Integer lamp_on;

    // должны ли быть двери открытыми или закрытыми
    // можно было бы принимать лямбдами (для дополнительных условий),
    // но это изврат (оверинжиниринг)
    Boolean door;
    Boolean window;

    // как физика узнает о том, что что-то произошло??????? 
    // у нее "есть" физика, которую он оповещает о изменениях состояния
    /**
     *
     *
     */
    public void setLamp(Boolean lamp) {
        rph.setLamp(lamp);
    }

    public void setCooler(Boolean cooler) {
        rph.setCooler(cooler);
    }

    public RoomControlCentre(RoomPhysicks rph, Integer t_on, Integer l_on, Boolean w, Boolean d) {
        this.rph = rph;
        temp_on = t_on;
        lamp_on = l_on;
        window = w;
        door = d;

        this.processor = new HashMap<>();
        applyProcessing();

        //processor.get("temp").apply(10);
    }

    private void applyProcessing() {
        processor.put("temp", new Function<Integer, Void>() {
            @Override
            public Void apply(Integer t) {
                System.out.println("log : apply temp");
                rph.setLamp(t > temp_on);
                return null;
            }
        });
        processor.put("light", new Function<Integer, Void>() {
            @Override
            public Void apply(Integer l)
            {
                System.out.println("log : apply light");
                rph.setLamp(l > lamp_on);
                return null;
            }          
        });
        processor.put("door", new Function<Integer, Void>() {
           @Override
           public Void apply(Integer d)
           {
               System.out.println("log : apply door");
               return null;
           }
        });
        processor.put("window", new Function<Integer, Void>() {
           @Override
           public Void apply(Integer d)
           {
               System.out.println("log : apply window");
               return null;
           }
        });

    }

    void recieve(String name, Integer value) {
        processor.get(name).apply(value);
    }

}
