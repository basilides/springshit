/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package springlab1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Sensor implements Runnable {
    // сенсор физически "есть" какой-то, сетеры тут - извращение относительно
    // физики процесса
    
    RoomPhysicks rp;
    RoomControlCentre rcc;
    String name;
    
    Sensor(String name, RoomControlCentre rcc, RoomPhysicks rp)
    {
        this.name = name;
        this.rcc = rcc;
        this.rp = rp;
    }
    public void notifyRcc(Integer value)
    {
        rcc.recieve(name, value);
    }
    
    @Override
    public void run() {
         Timer t = new Timer();

        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // измерение, в принципе, тоже можно обернуть 
                // вместе с "точностью" и т.д.
                notifyRcc(rp.measure(name));
                System.out.println("hue hue hue");
            }
        },
                0,          // задержка (не месячные)
                5 * 1000);  // перерыв
    }
}

// логично предположить, что мы можем объединить датчики в "клубок"

class SensorBunch {

    RoomControlCentre rcc;
    RoomPhysicks rph;

    List<Sensor> sensors;

    public SensorBunch(RoomPhysicks rph, RoomControlCentre rcc) {
        this.rph = rph;
        this.rcc = rcc;

        // полчаса я ебался до того как додумался что надо написать явное приведение типа
        // ПОСМОТРИТЕ НА МИНЯ Я ИСПОЛЬЗУЮ ПОЛЕМОРФИЗМ!!!!!
        // хуй они ничем не отличаются хули их делить
        
        // вместо православных списков инициализации - ублюдский asList
        // ява - это грубый, как кулак, ответ.
        sensors = new ArrayList<>(Arrays.asList(
                new Sensor("temp", rcc, rph),
                new Sensor("light", rcc, rph),
                new Sensor("door", rcc, rph),
                new Sensor("window", rcc, rph)));

        for (Sensor s : sensors) {
            s.run();
        }
    }

}
