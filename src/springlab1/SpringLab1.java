/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



package springlab1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author basilides
 */
public class SpringLab1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ApplicationContext context = 
                new ClassPathXmlApplicationContext("SpringXMLConfig.xml");        
        
        RoomControlCentre rcc = (RoomControlCentre) context.getBean("evilRoomControl");      
        
    }
    
}
